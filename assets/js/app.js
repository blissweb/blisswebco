import css from "../css/app.scss"

const Elm = require("../Elm/App.elm");
const mountNode = document.getElementById("elm-wrapper");

const ElmApp = Elm.App.embed(mountNode)
