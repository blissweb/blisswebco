defmodule BlisswebcoWeb.PageController do
  use BlisswebcoWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
