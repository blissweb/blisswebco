defmodule BlisswebcoWeb.PageControllerTest do
  use BlisswebcoWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Hello Blisswebco!"
  end
end
