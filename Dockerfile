FROM elixir:1.6.6-alpine

RUN mix local.hex --force
RUN mix local.rebar --force
RUN mkdir blisswebco
RUN rm -rf asssets
RUN rm -rf deps/*
RUN rm -rf _build/*
ADD . /blisswebco
WORKDIR /blisswebco
EXPOSE 4000
RUN MIX_ENV=prod mix deps.get
CMD MIX_ENV=prod PORT=4000 mix phx.server
